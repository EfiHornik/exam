@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name"> date </label>
            <input type = "Integer" class="form-control" name = "id">
        </div>     
        <div class="form-group">
            <label for = "description">Description</label>
            <input type = "text" class="form-control" name = "description">
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
