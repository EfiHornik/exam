@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>Candidate's Interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Description</th><th>Date</th><th>Candidate</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->description}}</td>
            <td>{{$interview->created_at}}</td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose Candidate
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($candidates as $candidate)
                      <a class="dropdown-item" href="#">{{$candidate->name}}</a>
                    @endforeach
                    </div>
                  </div>                
            </td>

                                                                        
        </tr>
    @endforeach
</table>
@endsection

