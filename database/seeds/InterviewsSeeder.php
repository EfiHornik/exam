<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'description' => Str::random(5).'Great Candidate',
            'created_at' => Carbon::now(),
        ]);
    }
}
